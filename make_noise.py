print("\n", "=" * 30)
import warnings
warnings.filterwarnings('ignore')
import os, sys, gc, pdb, argparse

import numpy as np
import keras
import tensorflow as tf
from keras.callbacks import ModelCheckpoint
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.layers import Lambda, Input, Dense, GlobalAveragePooling2D
from keras.models import Model
from keras.callbacks import LearningRateScheduler
from keras.optimizers import SGD

from art.classifiers import KerasClassifier
from art.attacks import UniversalPerturbation
from art.utils import random_sphere

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default='dataset/melanoma')
parser.add_argument('--model_weight', type=str, default='dataset/melanoma/model_1.h5')
parser.add_argument('--save_noise', type=str, default='dataset/melanoma/noise_v1.npy')
parser.add_argument('--gpu', type=str)
args = parser.parse_args()
# Select GPU
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]=args.gpu

# load data
X_train = np.load(args.dataset+'/X_train_art.npy')
Y_train = np.load(args.dataset+'/Y_train_art.npy')
X_train -= 128.0
X_train /= 128.0
eps = 10.0 / 128.0
fgsm_eps = 0.0024

if 'melanoma' in args.dataset:
    classes = 7
elif 'oct' in args.dataset:
    classes = 4
elif 'chestx' in args.dataset:
    classes = 2

if 'melanoma' in args.dataset:
    base_model = InceptionV3(weights='imagenet', include_top=False)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(classes, activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
else: # chestx, oct
    # 入力層を変更
    base_model = InceptionV3(weights='imagenet', include_top=False)
    base_model.layers.pop(0) # remove input layer
    newInput = Input(batch_shape=(None, 299,299,1))
    x = Lambda(lambda image: tf.image.grayscale_to_rgb(image))(newInput)
    tmp_out = base_model(x)
    tmpModel = Model(newInput, tmp_out)
    # 出力層を変更
    x = tmpModel.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(classes, activation='softmax')(x)
    model = Model(tmpModel.input, predictions)

for layer in model.layers:
    layer.trainable = True

sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
model.load_weights(args.model_weight)

# ART
classifier = KerasClassifier(model=model)

adv_crafter = UniversalPerturbation(
    classifier,
    attacker='fgsm',
    delta=0.000001,
    attacker_params={'eps':fgsm_eps},
    max_iter=20,
    eps=eps,
    norm=np.inf)

x_train_adv = adv_crafter.generate(X_train)
noise = adv_crafter.noise[0,:]

# 保存
np.save(args.save_noise, noise)
